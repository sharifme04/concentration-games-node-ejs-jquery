const express = require('express');
const path = require('path');
const expressLayouts = require('express-ejs-layouts');
const app = express();

app.use(expressLayouts);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));


app.get('/',(req, res)=>{
  res.render('home');
})

app.get('/game',(req, res)=>{
  res.render('games');
})

app.get('/instructions',(req, res)=>{
  res.render('instructions');
})

app.get('/aboutus',(req, res)=>{
  res.render('aboutUs');
})

app.listen(4000);
console.log('server 4000 port is running in backend');