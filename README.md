This project was bootstrapped with node js , ejs and jquery.

## Available Scripts

In the project directory, you can install node modules by `npm install` then run:

### `node server`

Runs the app in the development mode.<br>
Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

This is the first view after `node server` with welcome screen.
![screenshot](screenshots/3.PNG)

After clicking the `games` , you will see the games screen.
![screenshot](screenshots/4.PNG)

After clicking the `instructions` , you will see the instructions screen.
![screenshot](screenshots/5.PNG)

After clicking the `About us` , you will see the About us screen.
![screenshot](screenshots/6.PNG)
